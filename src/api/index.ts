import { Prisma } from "@prisma/client";
import prisma from "lib/prisma";

export const read = async () => {
  const posts = await prisma.post.findMany();

  prisma.$disconnect();
  return posts;
};

export const readById = async (uuid: string) => {
  const post = await prisma.post.findUnique({
    where: { uuid: uuid },
  });

  prisma.$disconnect();
  return post;
};

export const write = async (data: Prisma.postCreateInput) => {
  const post = await prisma.post.create({
    data: { title: data.title, content: data.content },
  });

  prisma.$disconnect();
  return post;
};

export const remove = async (uuid: string) => {
  const post = await prisma.post.delete({
    where: { uuid: uuid },
  });

  prisma.$disconnect();
  return post;
};

export const update = async (uuid: string, data: Prisma.postUpdateInput) => {
  const post = await prisma.post.update({
    where: { uuid: uuid },
    data: { title: data.title, content: data.content },
  });

  prisma.$disconnect();
  return post;
};
