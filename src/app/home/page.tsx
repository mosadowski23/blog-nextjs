import { read } from "@/api";

export const fetchCache = "auto",
  revalidate = 10;

export default async function Home() {
  const posts = await read();
  return (
    <div>
      {posts.map((post, index: number) => {
        return (
          <div key={index}>
            <div>{post.title}</div>
            <div>{post.content}</div>
          </div>
        );
      })}
    </div>
  );
}
